<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class URL extends Model
{
    protected $table = 'urls';

    //protected $dates = ['update_date', 'create_date', 'created_at', 'updated_at'];
    protected $fillable = [
    	'url', 
      'ip',
      'hosting_id',
    	'registrar', 
    	'update_date',
    	'create_date',
    	'registrant_name',
    	'registrant_organization',
    	'registrant_street',
    	'registrant_city',
    	'registrant_state',
    	'registrant_zip',
    	'registrant_phone',
    	'registrant_email',
    	'registrant_name_server_1',
    	'registrant_name_server_2',
    	'cpanel_login_url',
    	'cpanel_username',
    	'cpanel_password',
    	'ftp_login_address',
    	'ftp_username',
    	'ftp_password',
    	'wp_admin_username',
    	'wp_admin_password'
    	];


    public function users(){
      return $this->belongsToMany('\App\Models\User', 'user_url_pivot', 'url_id', 'user_id')->withTimestamps();
    }

    public function hosting(){
      $hosting = new \App\Models\SEO\Hosting;
      return $this->hosting_id ? $hosting->findOrFail($this->hosting_id) : null;
    }

    // public function URLIp(){
    //   $host = $this->url;
    //   $host=trim($host.'.'); // clean and add root .
      
    //   $ip= gethostbyname($host);
    //   if($ip==$host) $ip='No IP Found';// empty IP if there is no ip
    //   return $ip;
    // }

 /*------------------------
  MUTATORS AND ACCESSORS 
  ------------------------*/

  public function setUrlAttribute($value){
    $this->attributes['url'] = strtolower($value);
  }

  public function getUrlAttribute($value){
    return $this->attributes['url'];
  }

   public function setCpanelLoginUrlAttribute($value){
     $this->attributes['cpanel_login_url'] = \Crypt::encrypt($value); 
    }

    public function getCpanelLoginUrlAttribute($value){
      return $this->attributes['cpanel_login_url'] = \Crypt::decrypt($value); 
    }

	public function setCpanelUsernameAttribute($value){
     $this->attributes['cpanel_username'] = \Crypt::encrypt($value); 
    }

    public function getCpanelUsernameAttribute($value){
      return $this->attributes['cpanel_username'] = \Crypt::decrypt($value); 
    }

	public function setCpanelPasswordAttribute($value){
     $this->attributes['cpanel_password'] = \Crypt::encrypt($value); 
    }

    public function getCpanelPasswordAttribute($value){
      return $this->attributes['cpanel_password'] = \Crypt::decrypt($value); 
    }

	public function setFtpLoginAddressAttribute($value){
     $this->attributes['ftp_login_address'] = \Crypt::encrypt($value); 
    }

    public function getFtpLoginAddressAttribute($value){
      return $this->attributes['ftp_login_address'] = \Crypt::decrypt($value); 
    }

  	public function setFtpUsernameAttribute($value){
     $this->attributes['ftp_username'] = \Crypt::encrypt($value); 
    }

    public function getFtpUsernameAttribute($value){
      return $this->attributes['ftp_username'] = \Crypt::decrypt($value); 
    }

	public function setFtpPasswordAttribute($value){
     $this->attributes['ftp_password'] = \Crypt::encrypt($value); 
    }

    public function getFtpPasswordAttribute($value){
      return $this->attributes['ftp_password'] = \Crypt::decrypt($value); 
    }

    public function setWpAdminUsernameAttribute($value){
     $this->attributes['wp_admin_username'] = \Crypt::encrypt($value); 
    }

    public function getWpAdminUsernameAttribute($value){
      return $this->attributes['wp_admin_username'] = \Crypt::decrypt($value); 
    }

    public function setWpAdminPasswordAttribute($value){
     $this->attributes['wp_admin_password'] = \Crypt::encrypt($value); 
    }

    public function getWpAdminPasswordAttribute($value){
      return $this->attributes['wp_admin_password'] = \Crypt::decrypt($value); 
    }

    public function setUpdateDateAttribute($value){
      $date_parsed = $value ? \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s') : null;

      $this->attributes['update_date'] = $date_parsed ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_parsed) : null;
    }

    public function getUpdateDateAttribute($value){
      $date = $value ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value) : null;
      return $this->attributes['update_date'] = $date;
    }

    public function setCreateDateAttribute($value){
      $date_parsed = $value ? \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s') : null;

      $this->attributes['create_date'] = $date_parsed ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_parsed) : null;
    }

    public function getCreateDateAttribute($value){
        $date = $value ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value) : null;
      return $this->attributes['create_date'] = $date;  
    }
    
}

