<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class Whois extends Model
{
    protected $table="whois";

    protected $fillable=['username', 'password'];

    public function users(){
    	return $this->belongsToMany('\App\Models\User',  'user_whois', 'user_id', 'whois_id')->withTimestamps();
    }


    public function getUserAttribute(){
    	return $this->users()->first();
    }

    
    public function setUsernameAttribute($value){
    	$this->attributes['username'] =  \Crypt::encrypt($value); 
    }

    public function getUsernameAttribute($value){
      	return $this->attributes['username'] = \Crypt::decrypt($value); 
    }

    public function setPasswordAttribute($value){
    	$this->attributes['password'] =  \Crypt::encrypt($value); 
    }

    public function getPasswordAttribute($value){
      	return $this->attributes['password'] = \Crypt::decrypt($value); 
    }


}
