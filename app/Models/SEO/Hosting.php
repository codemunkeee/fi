<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class Hosting extends Model
{
    protected $table = 'hosting';

    protected $fillable = ['host_name', 'host_number', 'login_url', 'username', 'password'];


    public function users(){
    	return $this->belongsToMany('\App\Models\User', 'user_hosting_pivot', 'hosting_id', 'user_id')->withTimestamps();
    }
 

 /*------------------------
  MUTATORS AND ACCESSORS 
  ------------------------*/

    public function setPasswordAttribute($value){
     $this->attributes['password'] = \Crypt::encrypt($value); 
    }

    public function getPasswordAttribute($value){
      return $this->attributes['password'] = \Crypt::decrypt($value); 
    }

    public function setLoginUrlAttribute($value){
     $this->attributes['login_url'] = \Crypt::encrypt($value); 
    }

    public function getLoginUrlAttribute($value){
      return $this->attributes['login_url'] = \Crypt::decrypt($value); 
    }

    public function setUsernameAttribute($value){
     $this->attributes['username'] = \Crypt::encrypt($value); 
    }

    public function getUsernameAttribute($value){
      return $this->attributes['username'] = \Crypt::decrypt($value); 
    }

    public function setHostNameAttribute($value){
      $host_name = ucfirst(trim($value));

      $this->attributes['host_name'] = $host_name; 
    }

}
