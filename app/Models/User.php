<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['last_login'];


    /**
     * Get the user's confirmation status
     * 
     * @return HasOne ;returns the confirmation status of the user.
     */
    public function confirm(){
        return $this->hasOne('\App\Models\UserConfirmation', 'user_id');
    }

    /**
     * Get all roles from a particular user.
     * Uses role_user as a pivot table.
     * 
     * @return [type] belongsToMany [description] returns all roles that was assigned to a user.
     */
    public function roles()
    {
        //A user can have many roles.
        return $this->belongsToMany('\App\Models\Role', 'user_roles', 'user_id', 'role_id')->withTimestamps();
    }

    /**
     * Checks if user has a role given by $role_name
     * @param  string  $role_name [description] the role to be checked. (E.g., Admin, User, Moderator, etc.)
     * @return boolean            [description] returns true if $role_name exists, otherwise false.
     */
    public function is($role_name)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->role == $role_name)
            {
                return true;
            }
        }

        return false;
    }

    public function hostings(){
        return $this->belongsToMany('\App\Models\SEO\Hosting', 'user_hosting_pivot', 'user_id', 'hosting_id')->withTimestamps();
    }

    public function urls(){
        return $this->belongsToMany('\App\Models\SEO\URL', 'user_url_pivot', 'user_id', 'url_id')->withTimestamps();
    }

    public function whois()
    {
        return $this->belongsToMany('\App\Models\SEO\Whois', 'user_whois', 'user_id', 'whois_id')->withTimestamps();
    }

    // public function ownsUrl($value){

    // }
    
    public function getWhoisAttribute(){
        return $this->whois()->first();
    }
}
