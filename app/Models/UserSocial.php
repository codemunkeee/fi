<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
	/**
	* The database table used by the model.
	*
	* @var string
	*/
    protected $table = 'user_socials';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['social', 'user_social_id'];

	// /**
 //     *  
 //     * @return belongsTo returns the user associated with this social account.
 //     */
 //    public function user(){
 //        return $this->belongsTo('\App\Models\User', 'user_socials', 'user_id', 'social_id');
 //    }

}
