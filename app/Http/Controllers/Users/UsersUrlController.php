<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Users\StoreUserUrlRequest;
use App\Models\SEO\Hosting;
use App\Packages\SEO\SEO;
use App\Models\SEO\URL;


class UsersUrlController extends Controller
{   
    public function __construct(){
        $this->middleware('auth');        
    }

    public function index(){
        // $str = "https://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=facebook.com&username=Fireicetech One&password=Password123!&outputFormat=JSON";

        // $test =  urldecode ($str );
        // dd($test);

        $title  = 'urls';
        $items  = \Auth::user()->urls()->get();

        return view('seo.urls.index', compact('items', 'title'));
    }

    public function create(){

        $title      = 'urls';
        $method     = 'create';
        $disabled   = '';

        $host_names = \DB::table('user_hosting_pivot')
            ->select('hosting.*')
            ->leftJoin('users', 'user_hosting_pivot.user_id', '=', 'users.id')
            ->leftJoin('hosting', 'user_hosting_pivot.hosting_id', '=', 'hosting.id')
            ->where('users.id', '=', \Auth::user()->id)
            ->lists('hosting.host_name', 'hosting.id');

        $host_name = \Auth::user()->hostings()->first();

        return view('seo.urls.form', compact('title', 'method', 'disabled', 'host_names', 'host_name'));
    }

    public function store(StoreUserUrlRequest $request){

        //temp
        if (!isset(\Auth::user()->whois)) {
            return redirect('urls/create')->withErrors(['Whois data is not set up with this user.']);
        }

        $params = [
            'domain_name' => $request['url'],
            'username'    => \Auth::user()->whois->username,
            'password'    => \Auth::user()->whois->password
        ];

        //for testing :
        //$SEO = new SEO($params, true);
        $SEO = new SEO($params, true);
        $whois = $SEO->getWhois();
        if (is_null($whois)){
            \Flash::error($SEO->getErrorMessage());
            return redirect('/urls/create');
        } 

        //add new url to the database and add a pivot table
        \DB::transaction(function () use($request, $SEO) {

            
            $url = URL::create([
                'url'                       => $SEO->getURLRaw($request['url']), 
                'ip'                        => $SEO->getIP($request['url']),
                'hosting_id'                => (isset($request['hosting_id'])) ? intval($request['hosting_id']) : null,
                'registrar'                 => $SEO->getRegistrar(), 
                'update_date'               => $SEO->getUpdateDate(),
                'create_date'               => $SEO->getCreateDate(),
                'registrant_name'           => $SEO->getRegistrantName(),
                'registrant_organization'   => $SEO->getRegistrantOrganization(),
                'registrant_street'         => $SEO->getRegistrantStreet(),
                'registrant_city'           => $SEO->getRegistrantCity(),
                'registrant_state'          => $SEO->getRegistrantState(),
                'registrant_zip'            => $SEO->getRegistrantZip(),
                'registrant_phone'          => $SEO->getRegistrantPhone(),
                'registrant_email'          => $SEO->getRegistrantEmail(),
                'registrant_name_server_1'  => (null !== ($SEO->getNameServers())) ? $SEO->getNameServers()[0] : null,
                'registrant_name_server_2'  => (null !== ($SEO->getNameServers())) ? $SEO->getNameServers()[1] : null,
                'cpanel_login_url'          => (isset($request['cpanel_login_url'])) ? $request['cpanel_login_url'] : null,
                'cpanel_username'           => (isset($request['cpanel_username'])) ? $request['cpanel_username'] : null,
                'cpanel_password'           => (isset($request['cpanel_password'])) ? $request['cpanel_password'] : null,
                'ftp_login_address'         => (isset($request['ftp_login_address'])) ? $request['ftp_login_address'] : null,
                'ftp_username'              => (isset($request['ftp_username'])) ? $request['ftp_username'] : null,
                'ftp_password'              => (isset($request['ftp_password'])) ? $request['ftp_password'] : null,
                'wp_admin_username'         => (isset($request['wp_admin_username'])) ? $request['wp_admin_username'] : null,
                'wp_admin_password'         => (isset($request['wp_admin_password'])) ? $request['wp_admin_password'] : null
            ]);

            $url->users()->attach($url->id, ['user_id' => \Auth::user()->id]);

        });

        return redirect('urls');
    }


    public function show($id){
        // $t = \DB::table('hosting')->select(('CONCAT(hosting.host_name, " - ", hosting.host_number) AS a'));
        // dd($t);
        $title      = 'urls';
        $method     = 'show';
        $disabled   = 'disabled';

        $item = URL::findOrFail($id);

        $host_names = \DB::table('user_hosting_pivot')
            ->select('hosting.*', \DB::raw('CONCAT(hosting.host_name, " - ", hosting.host_number) AS host_name_num'))
            ->leftJoin('users', 'user_hosting_pivot.user_id', '=', 'users.id')
            ->leftJoin('hosting', 'user_hosting_pivot.hosting_id', '=', 'hosting.id')
            ->where('users.id', '=', \Auth::user()->id)
            ->lists('hosting.host_name_num', 'hosting.id');

        $host_name = $item->hosting();//Hosting::findOrFail($item->hosting_id);

        return view('seo.urls.form', compact('item', 'title', 'method', 'disabled', 'host_names', 'host_name'));

    }

    public function edit($id){
        $title      = 'urls';
        $method     = 'edit';
        $disabled   = '';

        $item = URL::findOrFail($id);
        
        $host_names = \DB::table('user_hosting_pivot')
            ->select('hosting.*', \DB::raw('CONCAT(hosting.host_name, " - ", hosting.host_number) AS host_name_num'))
            ->leftJoin('users', 'user_hosting_pivot.user_id', '=', 'users.id')
            ->leftJoin('hosting', 'user_hosting_pivot.hosting_id', '=', 'hosting.id')
            ->where('users.id', '=', \Auth::user()->id)
            ->lists('hosting.host_name_num', 'hosting.id');

        $host_name = $item->hosting();
        
        return view('seo.urls.form', compact('item', 'title', 'method', 'disabled', 'host_names', 'host_name'));
    }

    public function update(Request $request, $id){
        
        $URL = URL::findOrFail($id);
        $URL->update($request->all());

        \Flash::success('Updated successfully!');
        return redirect('/urls/'.$URL->id.'/edit');
        
    }   

    public function destroy($id){
        \DB::transaction(function () use($id) { 
            //delete pivot
            \DB::table('user_url_pivot')
                ->where('user_id', '=', \Auth::user()->id)
                ->where('url_id', '=', $id)
                ->delete();

            URL::destroy($id);
        });

        return redirect('/urls');        
    }
}
