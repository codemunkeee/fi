<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Users\StoreUserWhoisRequest;
use App\Validation\Users\UsersWhoisValidator;

use App\Models\SEO\Whois;


class UsersWhoisController extends Controller
{   
    public function __construct(){
        $this->middleware('auth');        
    }

    public function index(){
        $title  = 'whois';
        $items  = \Auth::user()->whois()->get();

        return view('seo.whois.index', compact('items', 'title'));
    }


    public function create(){
        $title      = 'whois';
        $method     = 'create';
        $disabled   = '';

        return view('seo.whois.form', compact('title', 'method', 'disabled'));
    }

    public function store(StoreUserWhoisRequest $request){
        //validation
        // $validator = $this->validate($request, [
        //     'username'    => 'required',
        //     'password'    => 'required'            
        // ]);
        
        //throw an error if validation fails
        // if (isset($validator)){
        //     if ($validator->fails()) {
        //         return redirect('/whois/create')
        //                 ->withErrors($validator)
        //                 ->withInput();
        //     }   
        // }

        //add new whois to the database and add a pivot table
        \DB::transaction(function () use($request) {
            $whois = Whois::create([
                        'username' => $request['username'],
                        'password' => $request['password']
                    ]);

            $whois->users()->attach($whois->id, ['user_id' => \Auth::user()->id]);
        });

        return redirect('whois');
    }


    public function show($id){
        $title      = 'whois';
        $method     = 'show';
        $disabled   = 'disabled';

        $item = Whois::findOrFail($id);

     
        return view('seo.whois.form', compact('item', 'title', 'method', 'disabled'));

    }

    public function edit($id){
        $title      = 'whois';
        $method     = 'edit';
        $disabled   = '';

        $item = Whois::findOrFail($id);
        
        return view('seo.whois.form', compact('item', 'title', 'method', 'disabled'));
    }

    public function update(Request $request, $id){
        
        $whois = Whois::findOrFail($id);
        $whois->update($request->all());

        \Flash::success('Updated successfully!');
        return redirect('/whois/'.$whois->id.'/edit');
        
    }   

 

}
