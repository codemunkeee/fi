<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;

class UsersController extends Controller
{

    public function __construct()
	{
		$this->middleware('auth');
		$this->roles = Role::all();

	}

	/**
	 * Show all users.
	 *
	 * @return Response
	 */
	public function index()
	{
		//redirect to homepage if the user's role is not admin
		if (!(\Auth::user()->is('Admin')))
		{
			return redirect('/');
		}		

		//else, get all users and display them on the view.
		$users = User::all();

		return view('system.users.index', compact('users'));
	}

	/**
	 * Shows the detailed information for a user.
	 * 
	 * @param  int $id  user's id
	 * @return view     user's profile
	 */
	public function show($id){
		// search for user by id
		$user = User::findOrFail($id);
		//dd($user->confirm());
		return view('system.users.show', compact('user'));
	}

	/**
	 * Shows an editable information for a user.
	 * 
	 * @param  int $id [description] user's id
	 * @return view    [description] returns an editable view for a user 
	 */
	public function edit($id){
		$user 		= User::findOrFail($id);
		$roles 		= $this->roles->lists('role', 'id');
		$user_roles = $user->roles()->lists('id')->toArray();

		return view('system.users.edit', compact('user', 'roles', 'user_roles'));
	}	

	/**
	 * Update a user.
	 * 
	 * @param  Request $request [description] the update request passed from the view, contains the new information from the user
	 * @param  int  $id      [description] user's id to be updated
	 * @return view           [description] redirects the user to the current view
	 */
	public function update(Request $request, $id){
		$user 				= User::findOrFail($id);
		$user->first_name 	= $request['first_name'];
		$user->last_name 	= $request['last_name'];
		
		//change user's role
		$user->roles()->sync($request['roles']);
	
		//change user's avatar
		if (($request->file('avatar'))){
            $avatar            	= $request->file('avatar');
            $destination_path   = 'assets/img/users/'.$user->id.'/';        
            $user->avatar    	= 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path .$avatar->getClientOriginalName();
            $request->file('avatar')->move($destination_path, $avatar->getClientOriginalName());     
        }
        $user->save();
		\Flash::success('User updated successfully!');
		return redirect('/users/'.$user->id.'/edit');
	}

	/**
	 * Removes a user from the system
	 * @param  Request $request [description] the delete request passed from the view
	 * @param  int  $id      [description] user's id to be destroyed
	 * @return view           [description] redirects the user to the users list
	 */
	public function destroy(Request $request, $id){
		User::destroy($id);
		\Flash::success('Successfully deleted user!');
		return redirect('/users');
	}

}
