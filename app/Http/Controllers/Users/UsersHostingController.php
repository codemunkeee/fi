<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SEO\Hosting;

class UsersHostingController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

 	public function index(){
    	$title 		= 'hosting';
    	$items = \Auth::user()->hostings()->get();

    	return view('seo.hosting.index', compact('items', 'title'));
    }

    public function create(){
    	$title 		= 'hosting';
    	$method		= 'create';
		$disabled 	= '';
    	
    	return view('seo.hosting.form', compact('title', 'method', 'disabled'));
    }

    public function store(Request $request){

    	//validation
    	$validator = $this->validate($request, [
            'host_name' => 'required|max:255',
            'login_url' => 'required|url',
			'username' => 'required',
			'password' => 'required'            
        ]);

    	//throw an error if validation fails
    	if (isset($validator)){
    	    if ($validator->fails()) {
        		return redirect('hosting/create')
                        ->withErrors($validator)
                        ->withInput();
        	}	
    	}

    	//add new hosting to the database and add a pivot table
		\DB::transaction(function () use($request) {
			//get the maximum count of this hostname
			$max = \Auth::user()->hostings()
				->where('host_name', $request['host_name'])
				->max('host_number') + 1;			 	

			$hosting = Hosting::create([
				'host_name' 	=> $request['host_name'],
				'host_number' 	=> $max,
				'login_url' 	=> $request['login_url'],
				'username'  	=> $request['username'],
				'password'  	=> $request['password']
			]);

			$hosting->users()->attach($hosting->id, ['user_id' => \Auth::user()->id]);
		});

		return redirect('hosting');
    }


    public function show($id){
    	$title 		= 'hosting';
    	$method		= 'show';
		$disabled 	= 'disabled';

    	$item = Hosting::findOrFail($id);

    	return view('seo.hosting.form', compact('item', 'title', 'method', 'disabled'));
    }

    public function edit($id){
    	$title 		= 'hosting';
    	$method		= 'edit';
		$disabled 	= '';

    	$item = Hosting::findOrFail($id);
    	return view('seo.hosting.form', compact('item', 'title', 'method', 'disabled'));
    }

    public function update(Request $request, $id){

    	$hosting = Hosting::findOrFail($id);

    	//get the maximum count of this hostname
		$max = \Auth::user()->hostings()
			->where('host_name', $request['host_name'])
			->max('host_number') + 1;

    	$hosting->host_name 	= $request['host_name'];
    	$hosting->host_number 	= $max;
     	$hosting->login_url 	= $request['login_url'];
    	$hosting->username  	= $request['username'];
    	$hosting->password  	= $request['password'];
    	$hosting->save();

    	\Flash::success('Hosting updated successfully!');
    	return redirect('/hosting/'.$hosting->id.'/edit');
    }   

    public function destroy($id){
        \DB::transaction(function () use($id) { 
            \DB::table('urls')->where('hosting_id', '=', $id)
                ->update(['hosting_id' => null]);
            //delete pivot
            \DB::table('user_hosting_pivot')
                ->where('user_id', '=', \Auth::user()->id)
                ->where('hosting_id', '=', $id)
                ->delete();

            Hosting::destroy($id);
        });

        return redirect('/hosting');   
    }
}
