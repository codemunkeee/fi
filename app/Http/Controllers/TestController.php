<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Socialite;
use App\Models\User;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('test');
        return view('welcome');
    }

    public function show($id){
        return 'hola from the other side';
    }
    public function login(){
        return view('login');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function callback($provider)
    {
        //get user social info
        $social_info = Socialite::driver($provider)->user();
        //check if user is already registered
        $user = User::whereEmail($social_info->getEmail())->first();
        
        if (!$user)
        {
            //if not, register the user 
            $user = new User;
            //todo -- use only 'name'. conflict with user schema.
            $user->first_name   = $social_info->getName();
            $user->email        = $social_info->getEmail();
            $user->avatar       = $social_info->getAvatar();
           //$user->save();            
        }

        //add to socials
        
        //login the user
        

        dd($user_info->user);
        // $user->token;
    }

}
