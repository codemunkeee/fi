<?php

namespace App\Http\Controllers\SEO;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SEO\Hosting;

use App\Packages\Encryption\Encryption;

class HostingController extends Controller
{
    public function index(){
    	$title = 'hosting';
    	$items = Hosting::all();
    	return view('seo.hosting.index', compact('items', 'title'));
    }

    public function create(){
    	$title 		= 'hosting';
    	$method		= 'create';
		$disabled 	= '';
    	
    	return view('seo.hosting.form', compact('title', 'method', 'disabled'));
    }

    public function store(Request $request){
    	//validate here
    	
    	$encryption = new Encryption(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
		$password_encrypted = $encryption->encrypt($request['password'], $key);
    	//dd($password_encrypted);

		$e2 = new Encryption(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
		$data = $e2->decrypt($password_encrypted, $key);
		dd($data);
    	//return Hosting::create($request->all());
    }


    public function show($id){
    	$title 		= 'hosting';
    	$method		= 'show';
		$disabled 	= 'disabled';

    	$item = Hosting::findOrFail($id);

    	return view('seo.hosting.form', compact('item', 'title', 'method', 'disabled'));
    }

    public function edit($id){
    	$title 		= 'hosting';
    	$method		= 'edit';
		$disabled 	= '';

    	$item = Hosting::findOrFail($id);
    	return view('seo.hosting.form', compact('item', 'title', 'method', 'disabled'));
    }

    public function update(Request $request, $id){
    	// encrypt passwords

    	$hosting = Hosting::findOrFail($id);

    	$hosting->host_name = $request['host_name'];
    	$hosting->login_url = $request['login_url'];
    	$hosting->username  = $request['username'];
    	$hosting->password  = $request['password'];
    	$hosting->save();

    	\Flash::success('Hosting updated successfully!');
    	//return view('seo.hosting.edit', compact('hosting'));
    }   

    public function destroy(Request $request, $id){


    	return $id;
    }

}
