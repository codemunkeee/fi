<?php

namespace App\Packages\SEO;

use DOMDocument;
use DOMXpath;

set_time_limit(0);

libxml_use_internal_errors(true);

Class FIScraper
{

  // public function __construct(){
  //   //$this->url = $url;
  //   $this->setHTML();
  //   $this->setDOM();
  //   $this->setXPath();
  // }

  public function setHTML(){
    //temp solution
    $this->html = file_get_contents($this->url);
  }
  // public function setHTML(){
  //   $ch = curl_init();
  //   curl_setopt($ch, CURLOPT_URL, $this->url);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //   curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
  //   $this->html = curl_exec($ch);

  //   curl_close($ch);
  // }

    public function getHTML(){
      return $this->html;
    }

    public function setDOM(){
      $this->dom = new DOMDocument();
      $this->dom->loadHTML($this->html);
      return $this->dom;
    }

    public function getDOM(){
      return $this->dom;
    }

    public function setXPath(){
      $this->xpath = new DOMXpath($this->dom);
    }

    public function getXPath(){
      return $this->xpath;
    }

}