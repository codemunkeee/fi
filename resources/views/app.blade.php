<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
		
		<title>Fire&IceTech</title>	
	    
	    <!-- Bootstrap Core CSS -->
	    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="/assets/css/sb-admin.css" rel="stylesheet">

  		<!-- jQuery -->
		<script src="/assets/js/jquery.js"></script>

	    <!-- Custom Fonts -->
	    <link href="/assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		@yield('header_scripts')

	</head>

	<body>
	 	<div id="wrapper">
			@include('core.header')
			
			@yield('content')

			@include('core.footer')
		
		    <!-- Bootstrap Core JavaScript -->
		    <script src="/assets/js/bootstrap.min.js"></script>

			@yield('footer_scripts')
		</div>
		<!-- /#wrapper -->
	</body>
</html>