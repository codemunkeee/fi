    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        Confirm your email! Click this <a href="/auth/confirm/resend/{{ $user->id }}">link</a>.
    </div>
