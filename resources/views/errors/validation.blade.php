@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<h3> Oops! Something went wrong! </h3>
        <hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif