@extends('app')

@section('header_scripts')
    <link href="/assets/css/datatables/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper">
    @include('errors.validation')
        <div class="container-fluid">
      <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> {{ ucfirst($title) }} <small> Overview </small></h1>
                    <ol class="breadcrumb">
                        <li class="active"> <i class="fa fa-users"></i> Item </li>
                        <li><a href='/{{ $title }}/create'><i class="fa fa-plus"></i> Create New</a></li>
                    </ol>

                    <table id="urls" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Password</th> 
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Usernmae</th>
                                <th>Password</th> 
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($items as $item)
                                <tr>    
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->password }}</td> 

                                    <td>
                                        {!! Form::open(['url'=>'/' . $title . '/'.$item->id, 'method'=>'DELETE', 'onsubmit'=>'return confirm("Are you sure you want to delete this?")']) !!}
                                            <div class="btn-group">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">                                     
                                                <a href="/{{ $title }}/{{ $item->id }}" class="btn btn-success"><i class="fa fa-eye" alt="View"></i></a>  
                                                        
                                                <a href="/{{ $title }}/{{ $item->id }}/edit" class="btn btn-warning"><i class="fa fa-edit" alt="Edit"></i></a>  
                                               <!--  <button type="submit" class="btn btn-danger"><i class="fa fa-times" alt="Delete"></i></button> -->
                                            </div>
                                                                                      
                                        {!! Form::close() !!}                                   
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
           
@endsection

@section('footer_scripts')

    <script src="/assets/js/datatables/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#urls').DataTable();
        } );

        // function deleteUser(id){
        //     var result  = confirm("Are you sure you want to delete this user?");
        //     var token   = $(this).data('token');
        //     if (result) {
        //         console.log(window.location.href + "/" + id);
        //         $.ajax({
        //           method: "POST",
        //           url: window.location.href + "/" + id,
        //           data: { _method : 'delete', _token : token }
        //         })
        //           .done(function() {
        //             alert( "Deleted successfully!" );
        //         });
        //     }       
        // }
    </script>

@endsection