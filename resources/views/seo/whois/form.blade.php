@extends('app')

@section('content')

	<div id="page-wrapper">
        @include('errors.validation')
        <div class="container-fluid">
        	@include('seo.whois.form_list')     
        </div>
    </div>


@endsection