<!-- Page Heading -->
<div class="row">
	<div class="col-lg-12">
    	<h1 class="page-header"> {{ ucfirst($title) }}<small> {{ $method }} </small> </h1>
    </div>
</div>

@if ($method == 'create')
	{!! Form::open(['url'=>'/' . $title , 'method'=>'POST']) !!}
@elseif ($method == 'edit' || $method == 'show')
	{!! Form::model($item,  ['url'=>'/' . $title . '/' . $item->id, 'method'=>'PUT']) !!}
@endif

        <input type="hidden" value="{{ csrf_token() }}"/>

        @if (isset(\Auth::user()->whois) && $method == 'create')
        	<div>Cannot add another {{$title}}</div>

	    @else
	    	<label>Whois Username: </label>{!! Form::text('username', null, ['placeholder' => 'Whois Username', 'class' => 'form-control', $disabled]) !!}
		    <label>Whois Password: </label>{!! Form::text('password', null, ['placeholder' => 'Whois Password', 'class' => 'form-control', $disabled]) !!}
			   	@if ($method != 'show')
			    	{!! Form::submit('Submit',['class'=> 'btn btn-primary', $disabled]) !!}	
			    @endif  
	    @endif
 	            	
{!! Form::close() !!}