<!-- Page Heading -->
<div class="row">
	<div class="col-lg-12">
    	<h1 class="page-header"> {{ ucfirst($title) }}<small> {{ $method }} </small> </h1>
    </div>
</div>

@if ($method == 'create')
	{!! Form::open(['url'=>'/hosting', 'method'=>'POST']) !!}
@elseif ($method == 'edit' || $method == 'show')
	{!! Form::model($item,  ['url'=>'/' . $title . '/' . $item->id, 'method'=>'PUT']) !!}
@endif

        <input type="hidden" value="{{ csrf_token() }}"/>

		<label>Hosting Name: </label>{!! Form::text('host_name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'required', $disabled ]) !!} 
		<label>Login URL: </label>{!! Form::text('login_url', null, ['placeholder' => 'Login URL', 'class' => 'form-control', 'required', $disabled]) !!}
		<label>Username: </label>{!! Form::text('username', null, ['placeholder' => 'Username', 'class' => 'form-control', 'required', $disabled]) !!}
	    <label>Password: </label>{!! Form::text('password', null, ['placeholder' => 'Password', 'class' => 'form-control', 'required', $disabled]) !!}
	    
	    @if ($method != 'show')
	    	{!! Form::submit('Submit',['class'=> 'btn btn-primary', $disabled]) !!}	
	    @endif    	            	
	    
{!! Form::close() !!}