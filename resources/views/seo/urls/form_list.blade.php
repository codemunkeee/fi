<!-- Page Heading -->
<div class="row">

	@if($method != 'create')
		<div class="col-lg-12">
		<div class="pull-left">
	    	<h1 class="page-header">{{ $item->url }}</h1>
	    	</div>
	    			@if ($method == 'show')
	        {!! Form::open(['url'=>'/' . $title . '/'.$item->id, 'method'=>'DELETE', 'onsubmit'=>'return confirm("Are you sure you want to delete this?")']) !!}
	        <div class="pull-right">
	        <div class="btn-group">
	        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			  
				<a href="/{{ $title }}/{{ $item->id }}/edit" class="btn btn-warning"><i class="fa fa-edit" alt="Edit"></i></a>  
	            
	            <button type="submit" class="btn btn-danger"><i class="fa fa-times" alt="Delete"></i></button>
	        </div>
	        </div>
	        {!! Form::close() !!} 
	    @endif
	    </div>

	@else
		<div class="col-lg-6">
	    	<h1 class="page-header"> {{ ucfirst($title) }}<small> {{ $method }}</small></h1>
	    </div>
    @endif


</div>

@if ($method == 'create')
	{!! Form::open(['url'=>'/' . $title , 'method'=>'POST']) !!}
@elseif ($method == 'edit' || $method == 'show')
	{!! Form::model($item,  ['url'=>'/' . $title . '/' . $item->id, 'method'=>'PUT']) !!}
@endif

        <input type="hidden" value="{{ csrf_token() }}"/>
       	@if ($method != 'show')
			<label>*URL: </label>{!! Form::text('url', null, ['placeholder' => 'URL (required)', 'class' => 'form-control', 'required', $disabled ]) !!} 
		@endif

		@if ($method != 'create')
			<label>IP Address: </label> <p>{{ $item->ip }}</p> 
		@endif
		<label>Host name : </label> {!! Form::select('hosting_id', $host_names, (isset($host_name)) ? $host_name->id : null, ['class' => 'form-control', 'placeholder' => 'Select Host Name', $disabled]) !!}
		<label>Cpanel Login URL: </label>{!! Form::text('cpanel_login_url', null, ['placeholder' => 'Cpanel Login URL', 'class' => 'form-control', $disabled]) !!}

		<label>Cpanel Username: </label>{!! Form::text('cpanel_username', null, ['placeholder' => 'Cpanel Username', 'class' => 'form-control', $disabled]) !!}
	    <label>Cpanel Password: </label>{!! Form::text('cpanel_password', null, ['placeholder' => 'Cpanel Password', 'class' => 'form-control', $disabled]) !!}
	    <label>FTP Login URL: </label>{!! Form::text('ftp_login_address', null, ['placeholder' => 'FTP Login URL', 'class' => 'form-control', $disabled]) !!}
		<label>FTP Username: </label>{!! Form::text('ftp_username', null, ['placeholder' => 'FTP Username', 'class' => 'form-control', $disabled]) !!}
	    <label>FTP Password: </label>{!! Form::text('ftp_password', null, ['placeholder' => 'FTP Password', 'class' => 'form-control', $disabled]) !!}
		<label>WP Admin Username: </label>{!! Form::text('wp_admin_username', null, ['placeholder' => 'WP Admin  Username', 'class' => 'form-control', $disabled]) !!}
	    <label>WP Admin Password: </label>{!! Form::text('wp_admin_password', null, ['placeholder' => 'WP Admin  Password', 'class' => 'form-control', $disabled]) !!}
	    
	    @if ($method != 'create')

		<div class="row">
			<div class="col-lg-12">
		    	<h1 class="page-content"><small> Other fields </small><h1>
		    </div>
		</div>

		<label>Registrar: </label>{!! Form::text('registrar', null, ['placeholder' => 'Registrar', 'class' => 'form-control', $disabled ]) !!} 

		<label>Update Date </label>{!! Form::input('date', 'update_date', $item->update_date ? $item->update_date->format('Y-m-d') : null, ['placeholder' => 'Update date:', 'class' => 'form-control', $disabled ]) !!}

		<label>Create Date </label>{!! Form::date('create_date', $item->create_date ? $item->create_date->format('Y-m-d') : null, ['placeholder' => 'Create date:', 'class' => 'form-control', $disabled ]) !!}
		
		<div class="row">
			<div class="col-lg-12">
		    	<h1 class="page-header"><small>Registrant Details</small><h1>
		    </div>
		</div>

		<label>Name: </label>{!! Form::text('registrant_name', null, ['placeholder' => 'Name', 'class' => 'form-control', $disabled ]) !!} 

		<label>Organization: </label>{!! Form::text('registrant_organization', null, ['placeholder' => 'organization', 'class' => 'form-control' , $disabled ]) !!}
		<label>Street </label>{!! Form::text('registrant_street', null, ['placeholder' => 'Street', 'class' => 'form-control', $disabled ]) !!}
		<label>City </label>{!! Form::text('registrant_city', null, ['placeholder' => 'City', 'class' => 'form-control', $disabled ]) !!}
		<label>State </label>{!! Form::text('registrant_state', null, ['placeholder' => 'State', 'class' => 'form-control', $disabled ]) !!}
		<label>Zip </label>{!! Form::text('registrant_zip', null, ['placeholder' => 'Zip', 'class' => 'form-control', $disabled ]) !!}
		<label>Phone </label>{!! Form::text('registrant_phone', null, ['placeholder' => 'Phone', 'class' => 'form-control', $disabled ]) !!}
		<label>Email </label>{!! Form::text('registrant_email', null, ['placeholder' => 'Email', 'class' => 'form-control', $disabled ]) !!}
		<label>Nameserver 1 </label>{!! Form::text('registrant_name_server_1', null, ['placeholder' => 'Name Server 1', 'class' => 'form-control', $disabled ]) !!}
		<label>Nameserver 2 </label>{!! Form::text('registrant_name_server_2', null, ['placeholder' => 'Name Server 2', 'class' => 'form-control', $disabled ]) !!}
		
		@endif

	    @if ($method != 'show')
	    	{!! Form::submit('Submit',['class'=> 'btn btn-primary', $disabled]) !!}	
	    @endif    	            	
	    
{!! Form::close() !!}