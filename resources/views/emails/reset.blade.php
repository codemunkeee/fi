<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Reset Your Password</h2>

        <div>
            {!! Html::link('/auth/password/reset/'.$token, 'Click here') !!} to reset your password.
                   
        </div>

    </body>
</html>