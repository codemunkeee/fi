@extends('app')

@section('header_scripts')
     <link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css">
    <script src="/assets/js/select2.min.js"></script>
@endsection

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <img src="{{ $user->avatar}}" alt="default image"  class="img-responsive" width="150" height="150">
                    <h1 class="page-header"> {{ $user->first_name }} {{ $user->last_name}} <small> Edit </small></h1>
                    <ol class="breadcrumb">
                        <li class="active"> <i class="fa fa-dashboard"></i> Edit User </li>
                    </ol>
                </div>
            </div>

            <div>
                {!! Form::model($user, ['url'=>'/users/'.$user->id, 'files'=>'true', 'method'=>'PUT']) !!}
                    <input type="hidden" value="{{ csrf_token() }}"/>
                    <div>
                        <label>First Name: {!! Form::text('first_name', null, ['class'=>'form-control']) !!} </label>
                        <label>Last Name: {!! Form::text('last_name',null, ['class'=>'form-control']) !!} </label> 
                        <label>Image : {!! Form::file('avatar', ['class'=>'form-control']) !!}</label> 
                    </div>
                    <div>
                    @if(\Auth::user()->is('Admin'))                       
                            <label> Roles :</label>{!! Form::select('roles[]', $roles, $user_roles, ['id'=>'roles', 'class'=>'form-control', 'multiple', 'required']) !!}                                          
                    @endif
                    {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                     </div>
                {!! Form::close() !!}
            </div>

        </div>
        <!-- /#page-wrapper -->
        
@endsection

@section('footer_scripts')  
    <script type="text/javascript">
      $('#roles').select2();
    </script>
@endsection