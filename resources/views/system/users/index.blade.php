@extends('app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> Users <small>Overview</small></h1>
                    <ol class="breadcrumb">
                        <li class="active"> <i class="fa fa-users"></i> Users </li>
                    </ol>
                    <table class="table table-hover">
                        <tr class="active">
                            <td>First Name</td>
                            <td>Last Name</td> 
                            <td>Email</td>
                            <td>Confirmed</td>
                            <td>Last Login</td>
                            <td>Actions</td>
                        </tr>

                        @foreach($users as $user)
                            <tr>    
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td> 
                                <td>{{ $user->email }}</td>
                                <td>{{ (($user->confirm()->first()->confirmed == 1) ? 'Yes' : 'No') }}</td>
                                <td>{{ $user->last_login->format('m/d/Y') }}</td>
                                <td>
                                    {!! Form::open(['url'=>'/users/'.$user->id, 'method'=>'DELETE', 'onsubmit'=>'return confirm("Are you sure you want to delete this user?")']) !!}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                                     
                                        <a href="/users/{{ $user->id }}" class="btn btn-success"><i class="fa fa-eye" alt="View"></i></a>  
                                        @if(!$user->is('Admin'))
                                            <a href="/users/{{ $user->id }}/edit" class="btn btn-warning"><i class="fa fa-edit" alt="Edit"></i></a>  
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-times" alt="Delete"></i></button>
                                        @endif 
                                                                      
                                    {!! Form::close() !!}                                   
                                <td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
        <!-- /#page-wrapper -->
        
@endsection

@section('footer_scripts')
    <script>
        function deleteUser(id){
            var result  = confirm("Are you sure you want to delete this user?");
            var token   = $(this).data('token');
            if (result) {
                console.log(window.location.href + "/" + id);
                $.ajax({
                  method: "POST",
                  url: window.location.href + "/" + id,
                  data: { _method : 'delete', _token : token }
                })
                  .done(function() {
                    alert( "User Deleted " );
                });
            }       
        }
    </script>

@endsection