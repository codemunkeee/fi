@extends('app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            
            @if((\Auth::user()->confirm()->first()->confirmed == 0) && (\Auth::user()->id === $user->id))
                @include('flashes.confirm')
            @endif
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <img src="{{ $user->avatar}}" alt="default image"  class="img-responsive" width="150" height="150">
                    <h1 class="page-header"> {{ $user->first_name }} {{ $user->last_name}} </h1>
                    <ol class="breadcrumb">
                        <li class="active"> <i class="fa fa-dashboard"></i> Users </li>
                    </ol>
                </div>
            </div>

            <div>
                <p> <label> First Name: </label>{{ $user->first_name }} </p> 
                <p> <label> Last Name: </label>{{ $user->last_name }} </p> 
                <p> <label> Last Login: </label>{{ $user->last_login }} </p> 
            </div>
        </div>
        <!-- /#page-wrapper -->
        
@endsection